require "diegodev9_palindrome/version"

module Diegodev9Palindrome

  # Returns true for a palindrome, false otherwise.
  def palindrome?
    if processed_content.empty?
      false
    else
      processed_content == processed_content.reverse
    end
  end

  private

    # Returns content for palindrome testing.
    def processed_content
      self.to_s.scan(/[a-z\w]/i).join.downcase
    end
end

class String
  include Diegodev9Palindrome
end

class Integer
  include Diegodev9Palindrome
end